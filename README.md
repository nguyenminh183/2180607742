# 2180607742

| Test Case ID | Test Objective | Precondition | Steps | Test Data | Expected Result | Post-condition |
|--------------|----------------|--------------|-------|-----------|-----------------|-----------------|
| TC001        | Successfully access academic results | Admin has logged into their account |  The admin clicks the "View Academic Results" button. | No additional data required. | The admin can access and view academic results. | The admin can continue using the system. |
| TC002        | Check for available academic results | Admin has logged into their account |  The admin clicks the "View Academic Results" button. | The admin has no academic results. | The system displays a message indicating that there are no available academic results to view. | The admin can continue using the system. |
| TC003        | View detailed information about a course | Admin has logged into their account and accessed academic results |  The admin clicks on the name of a course from the list of academic results. | Course data includes grades, course description, and other relevant information. | The system displays detailed information about the course, including grades, course description, and related information. | The admin can continue viewing academic results or return to the previous page. |

| Title | Admin Accesses Academic Results |
|:------|:---------------------------------|
|**Value Statement** | As an admin, <br>I want to access academic results after logging into my account, <br>so I can track academic progress. |
| **Acceptance Criteria** | **Acceptance Criterion 1:** <br> Given that the admin is logged into their account, <br> When they click the "View Academic Results" button, <br> Then the system checks their account and retrieves academic results from the school's server. <br> AND they are granted access to view academic results. |
|| **Acceptance Criterion 2:** <br> Given that the admin is logged into their account, <br> When they click the "View Academic Results" button, <br> Then the system checks their account and verifies the availability of academic results. <br> AND they are able to view academic results. |
|| **Acceptance Criterion 3:** <br> Given that the admin is logged into their account, <br> When they click on a specific course from the list of academic results, <br> Then the system displays detailed information about that course, including grades, course description, and any additional relevant data. |
| **Definition of Done** | - Acceptance Criteria Met <br> - Test Cases Passed <br> - Code Reviewed <br> - Product Owner Accepts User Story |
| **Owner** | Mr. Minh |
| **Iteration** | Unscheduled |
| **Estimate** | 5 Points |


![FormDiem](https://i.imgur.com/4LiEP5n.png)



# 2180608149


| Title | Lecturer update students's score into score sheet |
|:------|:--------------------------|
| Value Statement | As a Lecturer<br>I want to know my classes and my students's information that I'm teaching <br> So that I can update my students's score into the score sheet |
|Acceptance Criteria: | Given I have my classes and my students's infomation that I'm teaching in that semester<br><br>When I type in my students's score for each class <br>And I click the update button<br><br>Then I will have my students's component scores and final scores of each class saved successfully in the Database
|Definition of Done:  | Unit Tests Passed<br>Acceptance Criteria Met<br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirement Met<br>Product Owner Accepts User Story
Owner:| Mr.Trung
|Iteration: | Unscheduled
|Estimate: | 10 points

![Students_s_score_sheet](https://gitlab.com/2180608149/2180608149/uploads/ed7e12d15f1dfff6ed5afa241924b084/Students_s_score_sheet.png)

# 2180608143

| Title |admin login an account |
|:------|:--------------------------------|
|**value statement** |As admin, <br>I want  to log in to an account, <br>so I can access the student management program|
| **Acceptance Criteria**| **Acceptance Criterion 1 :**<br> Given that the account has been register.<br> When admin click the login or enter button.<br>Then They account will be checked on the school's server.<br> AND They will be allowed to access the student management software.|
||**Acceptance Criterion 2 :**<br> Given that the account has not registered <br> When admin click the login or enter button. <br> Then They account will be checked on the school's server <br> And ensure the rejection message is displayed under login button.<br> AND They will be not allowed to access the student management software.|
||**Acceptance Criterion 3 :**<br> Given that the account has been register.<br> And They forgot their login password.<br> When admin click the textlink "forgot the password?" under password text.<br>Then They will be redirected to the account support center page.|  
|**Definition of Done**|- Acceptance Criteria Met<br> - Test Cases Passed<br> - Package Into Specific Folders<br> - Synchronize Folders Between Devices<br> - Show Topic List<br> - Code Reviewed</br> - Product Owner Accepts User Story|
|**owner**| Nguyen Nhat Trinh owner|
|**Iteration**|Unscheluled|
|**Estiminate**|5Points|

![picture](https://gitlab.com/nhattrinhnguyen1601/2180608143/uploads/86dedcafdc6c8dffd732ec370ea520ae/image.png)

| Test case ID | Test Objective |Precondition|Step|Test Data|Expected Result|Actual Result|Pass/fail|
|:-------------|:---------------|:-----------|:---|:----|:-----|:-------|:--------|
|TC_01|successful user login|1.Valid user account|1.In the login Form, enter the usename.|"A valid username"|The username should appear in the "username" TextBox||pass|
|| | |2.The User enter password|"A valid password|The password should appear in the "password" TextBox| |pass|
|| | |3.CLick Login| |the user is logged and the program with go to homepage.| |pass|
|TC_02|unsuccessful user login|Invalid user account|1.In the login Form, enter the usename.|"A valid username"|The username should appear in the "username" TextBox||fail|
|| | |2.The User enter password|"A valid password|The password should appear in the "password" TextBox| |fail|
|| | |3.CLick Login| |A messageBox will be appear with "Invalid username or password"| |fail|
|TC_03|User forgot Account or password|Valid user account|In the login Form, Click "forgot account or password?"| |The program should redirect the user to the "Forgot account or password?" form where user can view help tips| |pass|


# 2180607988

| Title| Check and evaluate students training scores |
| ------ | ------ |
| **Value statement**    |  As a class officials      |
|        |  I want to check the training points of class members  |
| |so I can update training points for members|
|**Acceptace Criteria**|Given I have a list of training points of all members in that semester|
||And that semester ends|
||When I click check button|
||Then I can check whether a member's training points match the list or not|
||And I can edit points|
|**Definition of done**|Unit Tests Passed|
||Acceptace Criteria Met|
||Code Reviewed|
||Functional Tests Passed|
||Non-Functional Requirement Met|
||Product Owner Accepts User Story|
|**Owner**|Nguyen Ba Tam|
|***Interation**|Unscheduled|
|**Estimate**|5 points|

![Screenshot_2023-10-10_224514](https://gitlab.com/nguyenbatam/2180607988/uploads/5605ad869eb3fb0d978aa6d50ec6614f/Screenshot_2023-10-10_224514.png)


# thuy linh
| TiTel | Update personal information|
|-------| ---------------------------|
|**value statement** | As a student, I would like to update my personal information|
| ----- | --------------------------
| Acceptance Criteria | -Acceptance criterion 1:
|| given that update is true information
|| -when i enter the information
|| -and click the confirm information button
|| -and check again to make sure it is correct
|| -then i have successfully update the information.
|| -Acceptance criterion 2:
|| given that my information is not update
|| - when i enter the information,i didn't fill in the correct order and characters.
|| -and click the confirm information button
|| -then got an error
| Definition Of Done |- Unit tests passed |
| | - Students please change your personal information
|| - Student information changes are accepted
|Owner| Vu Thuy Linh
|Iteration| Unschedules
|Estimate| 5 Points
|Estimate: | 10 points|


![picture](https://gitlab.com/thuy-linh/thuy-linh/uploads/7cb562d8f353e3f68ed465b166d84d93/CHANGE.png)
